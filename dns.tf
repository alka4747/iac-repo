# Issue a Certificate and create 

# Create AWS SSL certificate
resource "aws_acm_certificate" "cert" {
  domain_name               = "alonk.xyz"
  subject_alternative_names = ["*.alonk.xyz"]
  validation_method         = "DNS"
}
# Create Route53 zone
resource "aws_route53_zone" "primary" {
  name = "alonk.xyz"
}

# Create records
resource "aws_route53_record" "domain_cert_validation_dns" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.primary.zone_id
}

# Validate the certificate
resource "aws_acm_certificate_validation" "eks_domain_cert_validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.domain_cert_validation_dns : record.fqdn]
}

# Import data
data "aws_elb_hosted_zone_id" "elb_zone_id" {}

data "kubernetes_service" "create-react-app" {
  metadata {
    name = "create-react-app-example"
  }
}
# Define DNS Records
resource "aws_route53_record" "eks_domain" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "alonk.xyz"
  type    = "A"

  alias {
    name                   = kubernetes_service.create-react-app.load_balancer_ingress[0].hostname
    zone_id                = data.aws_elb_hosted_zone_id.elb_zone_id.id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www" {
  zone_id        = aws_route53_zone.primary.zone_id
  name           = "www"
  type           = "CNAME"
  ttl            = "5"
  records        = ["alonk.xyz"]
}