# Infrastructure As Code Repo

This is an IaC repo for deploying a simple stateless web application on AWS

## Preface

In this section I will explain why did I choose each technology, and present the deployment architecture for the app.

### Tech Stack

Docker - Since the application to be deployed is a simple stateless web app, the easiest way to package it in a reproducible way would be inside a container

EKS - I don't want the deployment to be cloud-dependent, so I chose K8S as my container orchestration tool. Since I use AWS, I chose using EKS

Terraform - an IaC tool to provision the EKS cluster, and declare the yaml manifests deployed in each environment

### Deployment

The sample app's git repo is: https://gitlab.com/alka4747/create-react-app

The app has 2 environments - each one is stored in a seperate branch in this repo (master is production, staging is staging)

Staging environment is using a ClusterIP SVC only accessible internally and 1 replica of the app

Production environment is using a LoadBalancer SVC and 2 replicas of the app

Ideally the deployment pipeline would be:

Commited code in app repo -> CI/CD pipeline which replaces the name of the image in the k8s.tf file and triggers a terraform apply

In this way we keep this repo as a source of truth for our app infrastructure.

## Usage 

In order to access the staging environment after creating it, run the following command, after configuring the kubectl command

```
kubectl proxy --port=8080
http://localhost:8080/api/v1/namespaces/namespace_name/services/service_name:[port_name]/proxy
```

In order to access the production environment after creating it, you can access:
https://www.alonk.xyz


## Known drawbacks

* Most Terraform configuration is hard-coded
* Terraform state is not stored externally
* http traffic is allowed in production environment
* Did not implement CI/CD automation

## Useful Resources

During my work I used the following resources:
* https://learn.hashicorp.com/tutorials/terraform/eks
* https://itnext.io/build-an-eks-cluster-with-terraform-d35db8005963
* Terraform Docs