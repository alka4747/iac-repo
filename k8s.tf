# Declare yaml manifests using terraform's kubernetes provider

provider "kubernetes" {
  load_config_file       = "false"
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}
resource "kubernetes_deployment" "create-react-app" {
  metadata {
    name = "scalable-create-react-app-example"
    labels = {
      App = "ScalableCreateReactAppExample"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "ScalableCreateReactAppExample"
      }
    }
    template {
      metadata {
        labels = {
          App = "ScalableCreateReactAppExample"
        }
      }
      spec {
        container {
          image = "alka4747/create-react-app:latest"
          name  = "example"

          port {
            container_port = 8080
          }

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "create-react-app" {
  metadata {
    name = "create-react-app-example"
    annotations = {
      "external-dns.alpha.kubernetes.io/hostname"                     = "alonk.xyz",
      "service.beta.kubernetes.io/aws-load-balancer-backend-protocol" = "http",
      "service.beta.kubernetes.io/aws-load-balancer-ssl-cert"         = aws_acm_certificate.cert.id, 
      "service.beta.kubernetes.io/aws-load-balancer-ssl-ports"        = "https" 
    }
   }
  spec {
    selector = {
      App = kubernetes_deployment.create-react-app.spec.0.template.0.metadata[0].labels.App
    }
    port {
      name        = "http"
      port        = 80
      target_port = 8080
      protocol    = "TCP"
    }
    port {
      name        = "https"
      port        = 443
      target_port = 8080
      protocol    = "TCP"
    }
    type = "LoadBalancer"
  }
}